The documentation on this project (as provided by the upstrem author) is in the subdirectory "doc".
Best is to open the file "doc/html/index.html" in a web browser.

Copyright (C) 2006-2009,2017 Oliver Hamann.

Homepage: http://eaglemode.sourceforge.net/

License: GNU General Public License version 3 as published by the Free Software Foundation.


https://pkgs.org/search/?q=eaglemode

https://repology.org/projects/?search=eaglemode

https://launchpad.net/+search?field.text=eaglemode

https://github.com/search?q=eaglemode
https://github.com/ackalker/eaglemode (v0.94)
https://github.com/Osndok/eaglemode (v0.95

https://gitlab.com/meatcomputer/eaglemode (v0.94)

https://appimage.github.io/Eagle_Mode/
packaged as AppImage, by probonopd
.

#!/bin/bash
#     from:  https://github.com/probonopd/eaglemode/blob/master/AppRun
# eaglemode currently relies on EM_DIR to find its resources.
# The need for this environment variable should be removed by using a path
# relative to the main executable itself, e.g.,. by looking at 2readlink /proc/self/exe".
# In the meantime, this script can be used.
HERE="$(dirname "$(readlink -f "${0}")")"
EM_DIR="${HERE}/usr/lib/eaglemode/" exec "${HERE}/usr/bin/eaglemode" "$@"


___________
(un)related:
https://www.youtube.com/watch?v=_FjuPn7MXMs
2015: 3D "first person view" ui (compositor written using the QtCompositor AP)

_____________
(i did not) apt install  libx11-dev     libjpeg-dev  libpng-dev     libpoppler-glib-dev     libgtk2.0-dev   libfreetype6-dev  htmldoc
Building succeeded! (But you will have to live without: emAv, emPdf, emSvg)

mkdir -p /tmp/e/b
/tmp/e/make.pl build cpus=4
/tmp/e/make.pl install dir=/tmp/e/b menu=no
.###   41.3MB, 819 files
to launch:
/tmp/e/b/eaglemode.sh
.### ps_mem.py:  84.8 MiB + 488.0 KiB =  85.2 MiB   eaglemode


